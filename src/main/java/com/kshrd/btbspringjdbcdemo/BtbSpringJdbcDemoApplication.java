package com.kshrd.btbspringjdbcdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtbSpringJdbcDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtbSpringJdbcDemoApplication.class, args);
    }

}
