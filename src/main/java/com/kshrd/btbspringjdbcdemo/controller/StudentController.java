package com.kshrd.btbspringjdbcdemo.controller;


import com.kshrd.btbspringjdbcdemo.model.Student;
import com.kshrd.btbspringjdbcdemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {

    // Field Injection
    // Setter injection
    // Constructor injection
    @Autowired
    StudentRepository studentRepository;



    @GetMapping()
    public String index(){
        System.out.println("here is the value of the students ");
      //  studentRepository.getAllStudentOldWay().stream().forEach(System.out::println);
//        studentRepository.getAllStudents().stream().forEach(System.out::println);

        Student newStudent = new Student(8,"Visal","Male","I like learning Korea",2);
        System.out.println(" Find Student By Id ");

//        int insertResult = studentRepository.insertStudent(newStudent);
//        System.out.println(" Insert Result : "+insertResult);

        int updateResult = studentRepository.updateStudent(newStudent);
        System.out.println(" Insert Result : "+updateResult);

        System.out.println("Here Result:");
       System.out.println(studentRepository.findById(8));

       int deleteResult = studentRepository.deleteStudent(8);
        System.out.println("deleteResult: "+deleteResult);

        return "index";
    }

}
