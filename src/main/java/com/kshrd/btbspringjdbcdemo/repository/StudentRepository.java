package com.kshrd.btbspringjdbcdemo.repository;

import com.kshrd.btbspringjdbcdemo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate ;
    //jdbc normal
    // getAll student
   public List<Student> getAllStudentOldWay(){
        List<Student> studentList = new ArrayList<>();

        String username="postgres";
        String password="password";
        String url="jdbc:postgresql://localhost:5432/testdb";

        String query="select * from students";
        try(
                Connection connection = DriverManager.getConnection(url, username, password);
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                ){

            // mapping row

            while (rs.next()){
                studentList.add(
                        new Student(
                                rs.getInt("id"),
                                rs.getString("username"),
                                rs.getString("gender"),
                                rs.getString("bio"),
                                rs.getInt("course_id")
                        )
                );

            }

        }catch (SQLException ex){

            System.out.println("SQL Exception : "+ex.getMessage());
        }



        return studentList;

    }

    public Student mapToStudent(ResultSet rs, int rowNum) throws SQLException {
       return new Student(
               rs.getInt("id"),
               rs.getString("username"),
                rs.getString("gender"),
                rs.getString("bio"),
                rs.getInt("course_id"));
    }
    public List<Student>getAllStudents(){
     //  return jdbcTemplate.query("select * from students",new BeanPropertyRowMapper<>(Student.class));

//      return jdbcTemplate.query("select * from students", new RowMapper<Student>() {
//          @Override
//          public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
//              return new Student(
//                      rs.getInt("id"),
//                      rs.getString("username"),
//                      rs.getString("gender"),
//                      rs.getString("bio"),
//                      rs.getInt("course_id")
//              );
//          }
//      });

      //  return jdbcTemplate.query("select * from students",(rs,row)-> this.mapToStudent(rs,row));

        return jdbcTemplate.query("select * from students", this::mapToStudent);
    }
// find by id

    public  Student findById(int id){
       return jdbcTemplate.queryForObject("select * from students where id=?",new BeanPropertyRowMapper<>(Student.class), id);
    }
    // insert
    public int insertStudent(Student student){
       return jdbcTemplate.update("insert into students(id,username,gender,bio,course_id) values(?,?,?,?,?)",
               student.getId(),
               student.getUsername(),
               student.getGender(),
               student.getBio(),
               student.getCourse_id());
    }

    //update

    public int updateStudent(Student student){
       return jdbcTemplate.update("update students set username=?,gender=?,bio=?,course_id=? where id=?",
                student.getUsername(),
               student.getGender(),
               student.getBio(),
               student.getCourse_id(),
               student.getId()
               );
    }
    // delete

    public  int deleteStudent(int id ){

       return jdbcTemplate.update("delete from students where id=?",id);
    }


}
