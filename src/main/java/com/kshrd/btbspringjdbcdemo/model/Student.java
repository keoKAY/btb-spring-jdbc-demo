package com.kshrd.btbspringjdbcdemo.model;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Student {

    private  int id;
    private String username;
    private String gender;
    private String bio;
    private  int course_id;
}
